"use strict";

const app = require('./server/app');

app.listen(3000);

process.on("SIGINT", function exit() {
  process.exit();
});
