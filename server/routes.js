'use strict';
const debug = require('debug')('app:server');
const router = require('koa-router')();
const passport = require('koa-passport');

const account = require('./controllers/account');

router.get("/login", account.login);
router.get("/logout", account.logout);

// app.get('/login/auth/google', passport.authenticate('google', { scope: ['profile'] }));
// app.get('/login/auth/google/callback',
//   passport.authenticate('google', { failureRedirect: '/login' }),
//   function(req, res) {
//     // Successful authentication, redirect home.
//     res.redirect('/');
//   });

// app.get('/login/auth/facebook', passport.authenticate('facebook'));
// app.get('/login/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }),
//   function(req, res) {
//     // Successful authentication, redirect home.
//     res.redirect('/');
//   });

// app.post('/login', passport.authenticate('local', { failureRedirect: '/login' }),
//   function (req, res) {
//     res.redirect('/');
//   });

module.exports = (app) => {
  debug('attache routes middleware');
  app
    .use(router.routes())
    .use(router.allowedMethods());
};
