const path = require('path');
const debug = require('debug')('app:server');

/**
 * Apply Webpack HMR Middleware
 */
module.exports = (app) => {
  // if (project.env === 'development') {
  // TODO: needs enable only in development mode
  const project = require(path.resolve(process.cwd(), './client/config/project.config'));
  const webpack = require('webpack');
  const webpackConfig = require(path.resolve(process.cwd(), './client/config/webpack.config'));
  const compiler = webpack(webpackConfig);

  debug('Enabling webpack dev and HMR middleware');

  const middleware = require('koa-webpack');
  app.use(middleware({
    compiler: compiler,
    dev: {
      publicPath: webpackConfig.output.publicPath,
      contentBase: project.paths.client(),
      hot: true,
      quiet: project.compiler_quiet,
      noInfo: project.compiler_quiet,
      lazy: false,
      stats: project.compiler_stats
    }
  }));
}
