'use strict';
const debug = require('debug')('app:server');
const bodyParser = require('koa-bodyparser');

module.exports = (app) => {
  debug('attache bodyParser middleware');
  app.use(bodyParser());
}
