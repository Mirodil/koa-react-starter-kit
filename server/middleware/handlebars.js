'use strict';

const debug = require('debug')('app:server');
const hbs = require('koa-hbs');

/**
 * install handlebars template
 */
module.exports = (app, helpers) => {
  debug('attache handlebars middleware');

  app.use(hbs.middleware({
    viewPath: `${__dirname}/../views`,
    layoutsPath: `${__dirname}/../views/layouts`,
    partialsPath: `${__dirname}/../views/partials`,
    defaultLayout: 'main',
    helpers: helpers
  }));
}
