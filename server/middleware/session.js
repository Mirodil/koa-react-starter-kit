'use strict';
const debug = require('debug')('app:server');
const error = require('debug')('app:server:error');
const sessions = require('client-sessions');

// https://gist.github.com/netroy/b3e1497efa196872d0b0
const middleware = sessions({
  cookieName: 'SESSION',
  secret: 'Hello World',
  duration: 24 * 60 * 60 * 1000
});

function clientSession(req, res) {
  return new Promise((resolve, reject) => {

    middleware(req, res, err => {
      if (err)
        return reject(err);

      resolve();
    });

  });
}

module.exports = (app) => {
  debug('attache session middleware');
  app.use(async function (ctx, next) {
    try {
      await clientSession(ctx.request, ctx.response);

      Object.defineProperty(ctx, 'session', {
        get: function () { return ctx.request.session; }
      });

      await next();

    } catch (err) {
      error(err.message, err);
      ctx.body = { message: err.message };
      ctx.status = err.status || 500
    }
  });
}



// const session = require('koa-session-minimal');

// /**
//  * nodejs client cookie session store,
//  * support cross process and multi server without any other database(such as redis or mongodb)
//  * to store session data, cookie is encryption and md5 verify signatures.
//  *
//  * @param {Object} ops is option
//  *
//  * @example
//  * clientStore({
//  *  secret: 'blargadeeblargblarg', // should be a large unguessable string
//  *  duration: 24 * 60 * 60 * 1000, // how long the session will stay valid in ms
//  * })
//  */
// function clientStore(ops) {
//   return {

//     /**
//      * Attempt to fetch session by the given `sid`.
//      *
//      * @param {String} sid
//      * @param {Function} fn
//      * @api public
//      */
//     async get(sid) {
//       throw new Error('Not implemented exception');
//     },

//     /**
//      * Commit the given `sess` object associated with the given `sid`.
//      *
//      * @param {String} sid
//      * @param {Session} sess
//      * @api public
//      */
//     async set(sid, sess, ttl) {
//       throw new Error('Not implemented exception');
//     },

//     /**
//      * Destroy the session associated with the given `sid`.
//      *
//      * @param {String} sid
//      * @api public
//      */
//     async destroy(sid) {
//       throw new Error('Not implemented exception');
//     }

//   }
// }
