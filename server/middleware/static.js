'use strict';

const path = require('path');
const serve = require('koa-static');
const debug = require('debug')('app:server');

// Serve static assets from ~/public since Webpack is unaware of
// these files. This middleware doesn't need to be enabled outside
// of development since this directory will be copied into ~/dist
// when the application is compiled.
module.exports = (app) => {
  debug('attache static middleware')
  app.use(serve(path.resolve(process.cwd(), './public')));
}

