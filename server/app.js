'use strict';

const Koa = require('koa');

const debug = require('debug')('app:server');
// const compress = require('compression');

const app = new Koa();

// ------------------------------------
// Apply koa middlewares
// ------------------------------------
require('./middleware/webpack')(app);
require('./middleware/handlebars')(app);
require('./middleware/static')(app);
require('./middleware/session')(app);
require('./middleware/bodyparser')(app);
require('./routes')(app);

module.exports = app;
