'use strict';

module.exports.index = async function index() {

  await this.render("index", {
    title: 'Site name',
    user: this.session.passport.user || {}
  });
};
