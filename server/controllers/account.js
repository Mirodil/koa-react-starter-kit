'use strict';

module.exports.login = async function login(ctx) {
  await ctx.render('login', { user: {} });
};

module.exports.logout =async function logout(ctx) {
  ctx.logout();
  await ctx.redirect('/');
};
